﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MVA_OOP
{
    public class Student : Person
    {
        private List<double> grades = new List<double>();
        public List<double> Grades
        {
            get { return grades; }
        }

        public int TotalPoints { get => totalPoints; set => totalPoints = value; }
        private int totalPoints;

        public Student(int Id, string name)
        {
            this.Id = Id;
            this.name = name;
        }

        public Student(int Id, string name, string expertise)
        {
            this.Id = Id;
            this.name = name;
            this.expertise = expertise;
        }

        override public void AddClass(string className)
        {
            if (!classes.Contains(className) && classes.Count < 2)
            {
                classes.Add(className);
            }
            else
            {
                throw new InvalidOperationException("TAs may only teach 2 classes.");
            }
        }

        public void AddCourse(int grade)
        {
            if (grade >= 4 && grade <= 10)
            {
                grades.Add( grade);
            }            
        }

        public double CalculateGPA()
        {
            double sum = 0;
            for (int i = 0; i < grades.Count; i++)
            {
                sum += grades[i];
            }
            return sum / grades.Count;
        }

        internal void AssignGrade(string assignment, int points)
        {
            totalPoints += points;
        }
    }
}
