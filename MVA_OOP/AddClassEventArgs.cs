﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVA_OOP
{
    public class AddClassEventArgs : EventArgs
    {
        public string ClassTitle { get; }
        public string Subject { get; }
        public AddClassEventArgs(string title, string subject)
        {
            this.ClassTitle = title;
            this.Subject = subject;
        }
    }
}
