﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVA_OOP
{
    public class ClassOffering
    {
        private readonly Teacher leadInstructor;
        private readonly List<Student> TAs = new List<Student>();
        private readonly List<Student> students = new List<Student>();
        private readonly Func<string, string, int> gradingFunc;

        public ClassOffering(Teacher leadInstructor, Func<string, string, int> gradingFunc)
        {
            this.leadInstructor = leadInstructor;
            this.gradingFunc = gradingFunc;
        }

        public void AddStudent(Student enrollee)
        {
            students.Add(enrollee);
        }

        public void SubmitAssignment(Student enrollee, string assignment, string result)
        {
            var points = gradingFunc(assignment, result);
            enrollee.AssignGrade(assignment, points);
        }

        public double AverageGrade()
        {
            double sum = 0;
            for (int i = 0; i < students.Count; i++)
            {
                sum += students[i].TotalPoints;
            }
            return sum / students.Count;
        }
    }
}
