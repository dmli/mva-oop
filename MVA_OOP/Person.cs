﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MVA_OOP
{
    public abstract class Person
    {
        protected string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }
        protected string expertise;
        public string Expertise
        {
            get { return expertise; }
            set { expertise = value; }
        }
        protected int Id { get; set; }
        public int ID
        {
            get { return Id; }
            protected set { Id = value; } // set is protected so that the ID cannot be changed afterwards
        }

        protected List<string> classes = new List<string>();
        public List<string> Classes
        {
            get { return classes; }
        }

        abstract public void AddClass(string className);
        public void RemoveClass(string className)
        {
            classes.Remove(className);
        }
    }
}
