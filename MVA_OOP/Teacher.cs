﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MVA_OOP
{
    public class Teacher : Person
    {
        public event EventHandler<AddClassEventArgs> OnClassAdded;
        public Teacher(int Id, string name, string expertise)
        {
            this.Id = Id;
            this.name = name;
            this.expertise = expertise;
        }

        override public void AddClass(string className)
        {
            if (!classes.Contains(className))
            {
                classes.Add(className);
                OnClassAdded?.Invoke(this, new AddClassEventArgs(className, this.Expertise));
                var offering = new ClassOffering(this, (assignment, work) => 1);
            }            
        }
    }
}
