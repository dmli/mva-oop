﻿using MVA_OOP;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Student test1 = new Student(100, "Sergei");
            Console.WriteLine(test1.ID + " and the student's name is: " + test1.Name);
            test1.AddCourse(4);
            test1.AddCourse(10);
            test1.AddCourse(6);
            test1.AddCourse(7);
            test1.AddCourse(8);
            test1.AddCourse(6);
            Console.WriteLine("Student's GPA is: " + test1.CalculateGPA());
            List<double> test1grades = new List<double>(test1.Grades);
            foreach (double number in test1grades)
            {
                Console.WriteLine($"{number} ");
            }
            
            Teacher test2 = new Teacher(101, "John", "Mathematics");
            Console.WriteLine("Teacher " + test2.Name + "'s ID is " + test2.ID + " and his field is " + test2.Expertise + ".");
            test2.AddClass("Integrals");
            test2.AddClass("Algebra");
            test2.AddClass("Statistics");
            List<string> test2classes = new List<string>(test2.Classes);
            foreach (string className in test2classes)
            {
                Console.WriteLine(className);
            }

            Student test3 = new Student(102, "Brausni", "IT");
            Console.WriteLine("TA " + test3.Name + "'s ID is " + test3.ID + " and his field is " + test3.Expertise + ".");
            test3.AddClass("IT Basics");
            test3.AddClass("IT Fundamentals");
            try
            {
                test3.AddClass("IT Advanced Techniques");
            }
            catch (InvalidOperationException e)
            {
                Console.WriteLine(e);
            }
            List<string> test3classes = new List<string>(test3.Classes);
            foreach (string className in test3classes)
            {
                Console.WriteLine(className); //should not print IT Advanced Techniques class
            }
            test3.RemoveClass("IT Fundamentals");
            test3.AddClass("IT Advanced Techniques");
            test3classes = test3.Classes;
            foreach (string className in test3classes)
            {
                Console.WriteLine(className); //should not print IT Fundamentals class
            }
            ClassOffering newClass = new ClassOffering(test2, gradingFunction);
            newClass.AddStudent(test1);
            newClass.AddStudent(test3);
            newClass.SubmitAssignment(test1, "exam", "pass");
            newClass.SubmitAssignment(test1, "exam", "pass");
            newClass.SubmitAssignment(test1, "exam", "pass");
            newClass.SubmitAssignment(test3, "exam", "pass");
            newClass.SubmitAssignment(test3, "exam", "pass");
            Console.WriteLine(test1.TotalPoints);

            List<Student> students = new List<Student>();
            students.Add(test1);
            students.Add(test3);
            test3.AddCourse(5);
            test3.AddCourse(7);
            test3.AddCourse(4);
            Student test4 = new Student(104, "Boris");
            Student test5 = new Student(105, "Igor");
            students.Add(test4);
            test4.AddCourse(6);
            test4.AddCourse(9);
            test4.AddCourse(8);
            students.Add(test5);
            test5.AddCourse(10);
            test5.AddCourse(10);
            test5.AddCourse(9);
            for (int i = 0; i < students.Count; i++)
            {
                Console.WriteLine(students[i].Name);
            }
            students.Sort((x, y) => x.Name.CompareTo(y.Name));
            Console.WriteLine("Student list sorted alphabetically.");
            for (int i = 0; i < students.Count; i++)
            {
                Console.WriteLine(students[i].Name);
            }
            students.Sort((y, x) => x.CalculateGPA().CompareTo(y.CalculateGPA()));
            for (int i = 0; i < students.Count; i++)
            {
                Console.WriteLine(students[i].Name + "'s GPA is " + students[i].CalculateGPA());
            }
            Console.WriteLine("Student list sorted by GPA.");
            Console.WriteLine("Average points in class were: " + newClass.AverageGrade());
            Console.ReadLine();
        }

        private static int gradingFunction(string arg1, string arg2)
        {
            return 1;
        }
    }
}
